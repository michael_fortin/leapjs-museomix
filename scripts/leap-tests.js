// Author: Michaël Fortin (Simbioz)
// Date: Nov 9, 2013
//
// Notes:
//   - The local leap.js file is the latest leap JS API as of now
//   - Same for JQuery
//   - linq.js reference: http://neue.cc/reference.htm
//     (in essence, wrap in Enumerable, process, convert to js Array using .ToArray())

var LEAP_PINCH_THRESHOLD_IN_MILLIMETERS = 80;

var LEAP_LEFT_HAND = "LEAP_LEFT_HAND";
var LEAP_RIGHT_HAND = "LEAP_RIGHT_HAND";

$(function() {
	var frameCounter = 0;
	var handKindMapping = {};
	
	var options = { host: '127.0.0.1', port: 6437, enableGestures: false };
	var controller = new Leap.Controller(options);
	controller.connect();
	
	controller.on('frame', function(frame) {
		
		// Filter out hand false positives to eliminate noise in the data
		var hands = filterOutHandFalsePositives(frame.hands);
		
		// If the hand array is empty, flush the hand kind mapping values to start fresh
		if (hands.length == 0) handKindMapping = {};
		
		// Output general info to HTML
		var frameInfo = 'Frame {0}, {1} hands (post-filtering), {2} fingers';
		var numberOfHands = hands.length;
		var numberOfFingers = Enumerable.From(hands).Sum('$.fingers.length');
		$('#frame-info').html(String.format(frameInfo, ++frameCounter, numberOfHands, numberOfFingers));
		
		// Identify hands (left or right)
		var handIndex = discriminateLeftHandFromRightHand(hands);
		
		// Output hand identification info to HTML
		outputHandInfo('#left-hand-info', handIndex[LEAP_LEFT_HAND]);
		outputHandInfo('#right-hand-info', handIndex[LEAP_RIGHT_HAND]);
	});
	
	/* Filtering and Gestures */
	
	function filterOutHandFalsePositives(hands) {
		// Change this to correspond to the chosen interaction method.
		// Currently, we simply discard hands with one finger or less.
		return Enumerable.From(hands).Where('$.fingers.length > 1').ToArray();
	}
	
	function discriminateLeftHandFromRightHand(hands) {
		// No hand, return a valid but empty dictionary
		if (hands.length == 0)
			return { LEAP_LEFT_HAND : null, LEAP_RIGHT_HAND : null };
		
		// If there's only one hand, consider its horizontal position from the center to infer which one it is
		if (hands.length == 1)
		{
			var inferredKind;
			var hand = hands[0];
			
			var handKind = handKindMapping[hand.id];
			if (handKind)
			{
				// If our index already has a hand-id / hand kind mapping, don't infer
				// the kind again and simply consider that the kind didn't change.
				inferredKind = handKind;
			}
			else
			{
				// Infer the hand's kind based on its horizontal position from the center
				inferredKind = (hand.stabilizedPalmPosition[0] < 0) ? LEAP_LEFT_HAND : LEAP_RIGHT_HAND;
				
				// Store the hand association that we inferred so that we can keep that association if the hand crosses the "zero barrier"
				handKindMapping[hands[0].id] = inferredKind;
			}
			
			return { LEAP_LEFT_HAND : (inferredKind == LEAP_LEFT_HAND ? hand : null),
				     LEAP_RIGHT_HAND : (inferredKind == LEAP_RIGHT_HAND ? hand : null)};
		}
		
		// If there's two hands, order hands by their horizontal position to find the leftmost and rightmost hand
		var orderedHands = Enumerable.From(hands).OrderBy('$.stabilizedPalmPosition[0]').ToArray();
		return { LEAP_LEFT_HAND : orderedHands[0], LEAP_RIGHT_HAND : orderedHands[1] };
	}
	
	function detectPinchGesture(hand) {
		var fingers = getThumbAndIndexFingers(hand);
		if (fingers.length < 2) return false;
		
		var distanceBetweenFingers = getDistanceBetweenFingers(fingers[0], fingers[1]);
		return distanceBetweenFingers <= LEAP_PINCH_THRESHOLD_IN_MILLIMETERS;
	}
	
	/* Helper Functions */
	
	function getHandKind(hand) {
		return handKindMapping[hand.id];
	}
	
	function getThumbAndIndexFingers(hand) {
		var kind = getHandKind(hand);
		var orderedFingersEnumerable = Enumerable.From(hand.fingers).OrderBy('$.tipPosition[0]');
		
		if (kind == LEAP_LEFT_HAND)
		{
			return orderedFingersEnumerable.TakeFromLast(2).ToArray();
		}
		else
		{
			return orderedFingersEnumerable.Take(2).ToArray();
		}
	}
	
	function getDistanceBetweenFingers(finger1, finger2) {
		var xDistance = finger2.tipPosition[0] - finger1.tipPosition[0];
		var yDistance = finger2.tipPosition[1] - finger1.tipPosition[1];
		var zDistance = finger2.tipPosition[2] - finger1.tipPosition[2];
		return Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2) + Math.pow(zDistance, 2));
	}
	
	/* HTML Output */
	
	function outputHandInfo(selector, hand) {
		if (!hand)
		{
			$(selector).html("Not detected");
			return;
		}
		
		var handInfoFormat = "<u>Distance between thumb and index:</u> {0}<br/>";
		handInfoFormat += "<u>Pinching:</u> {1}<br/>";
		handInfoFormat += "<u>Position:</u><br/>{2}<br/>";
		
		var fingers = getThumbAndIndexFingers(hand);
		var distance = fingers.length >= 2 ? getDistanceBetweenFingers(fingers[0], fingers[1]) : Number.MAX_VALUE;
		
		var pinching = detectPinchGesture(hand);
		
		var xPos = hand.stabilizedPalmPosition[0];
		var yPos = hand.stabilizedPalmPosition[1];
		var zPos = hand.stabilizedPalmPosition[2];
		var positionString = String.format("&nbsp;{0}<br/> &nbsp;{1}<br/> &nbsp;{2}<br/>", xPos, yPos, zPos);
		
		$(selector).html(String.format(handInfoFormat, distance, yesOrNo(pinching), positionString));
	}
});