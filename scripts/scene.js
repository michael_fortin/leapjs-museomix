// Author: Michaël Fortin (Simbioz)
// Date: Nov 9, 2013
//
// Three.js https://github.com/mrdoob/three.js#readme
// linq.js http://linqjs.codeplex.com/

var LEAP_LEFT_HAND = "LEAP_LEFT_HAND";
var LEAP_RIGHT_HAND = "LEAP_RIGHT_HAND";

// Leap hand position offsets to apply to incoming data to compensate for the fact that we interact
// with the right hand and that we use the palm's position to move the pointer
var LEAP_X_OFFSET = 0.3;
var LEAP_Z_OFFSET = -1;

var LEAP_PINCH_THRESHOLD_IN_MILLIMETERS = 80; // Distance between fingers below which pinch is active
var LEAP_PINCH_HYSTERESIS_MARGIN = 15; // The hysteresis margin prevents accidental releases of objects when pinching
var LEAP_MOVEMENT_SPEED_FACTOR = 0.06; // The data the Leap outputs is in millimeters, values way too big values for our 3D scene (with current setup)
var OBJECTS_START_SPREAD = 5; // Horizontal spread of manipulatable objects from the center
var OBJECTS_START_Z_POS = -3.5; // Z offset of the objects when loaded (higher value moves them towards the top of the screen)
var OBJECT_SCALE_SELECTED = 0.000082; // Scale of objects when selected. Compensates for collada model sizes that are enormous.
var OBJECT_SCALE_UNSELECTED = 0.00008; // Scale of objects when unselected. Compensates for collada model sizes that are enormous.
var POINTER_COLLISION_DISTANCE_THRESHOLD = 1.3; // Distance from an object's center below which the pointer is considered to collide with the object
var POINTER_COLOR = 0xffbd00;
var POINTER_SIZE = 0.3; // Dimensions of the pointer cube
var POINTER_AXIS_SIZE = 0.05; // Dimensions of the pointer cube

$(function() {
	var handKindMapping = {};
	var meshes = [];
	var previouslyCollidingMesh;
	var pinching;
	var selectedMesh;
	var pointer;
	var pointerAxisX;
	var pointerAxisZ;
	
	var scene = new THREE.Scene();
	
	var camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.1, 1000);
	camera.position.y = 4;
	camera.position.z = 4;
	camera.rotation.x = -(Math.PI / 4);
	
	var renderer = new THREE.WebGLRenderer();
	renderer.setSize(window.innerWidth, window.innerHeight);
	
	document.body.appendChild(renderer.domElement);
	
	createPointer(); // Create the pointer that will follow the right hand
	loadMeshes(["1", "2", "3", "4", "5", "6"]);
	
	// Start the rendering loop (the "render" function is called for each rendering frame)
	render();
	
	// Configure and start the Leap frame loop
	var options = { host: '127.0.0.1', port: 6437, enableGestures: false };
	var controller = new Leap.Controller(options);
	controller.connect();
	
	controller.on('frame', function(frame) {
	
		// Filter out hand false positives to eliminate noise in the data
		var hands = filterOutHandFalsePositives(frame.hands);
	
		// If the hand array is empty, flush the hand kind mapping values to start fresh
		if (hands.length == 0) handKindMapping = {};
	
		// Identify hands (left or right)
		var handIndex = discriminateLeftHandFromRightHand(hands);
		
		var hand = handIndex[LEAP_RIGHT_HAND];
		if (hand)
		{
			var wasPinching = pinching;
			pinching = isPinching(hand);
			
			// Position the pointer
			var xPos = hand.stabilizedPalmPosition[0] * LEAP_MOVEMENT_SPEED_FACTOR + LEAP_X_OFFSET;
			var zPos = hand.stabilizedPalmPosition[2] * LEAP_MOVEMENT_SPEED_FACTOR + LEAP_Z_OFFSET;
			positionMeshOn2DPlane(pointer, xPos, zPos);
			positionMeshOn2DPlane(pointerAxisX, xPos, zPos);
			positionMeshOn2DPlane(pointerAxisZ, xPos, zPos);
			scaleMesh(pointer, pinching ? 0.6 : 1.0);
			
			// Scale the mesh we're currently pointing to indicate that it can be selected
			var collidingMesh = findCollidingMesh(pointer);
			scaleMesh(previouslyCollidingMesh, OBJECT_SCALE_UNSELECTED);
			scaleMesh(collidingMesh, OBJECT_SCALE_SELECTED);
			previouslyCollidingMesh = collidingMesh;
			
			if (!pinching && wasPinching)
			{
				selectedMesh = null
			}
			else if (pinching && !wasPinching && collidingMesh)
			{
				selectedMesh = collidingMesh;
			}
			
			positionMeshOn2DPlane(selectedMesh, xPos, zPos);
		}
	});
	
	/* ####################################### Three.js (3D Scene) ######################################## */

	/* Rendering */

	function render() {
		requestAnimationFrame(render);
		renderer.render(scene, camera);
	}

	/* Mesh Creation */

	function createPointer() {
		pointer = createCube(POINTER_COLOR, POINTER_SIZE, 1);
		pointerAxisX = createLine("x", POINTER_COLOR, POINTER_AXIS_SIZE, 0.2);
		pointerAxisZ = createLine("z", POINTER_COLOR, POINTER_AXIS_SIZE, 0.2);
	}
	
	function loadMeshes(names)
	{
		for (var i = 0; i < names.length; i++)
			var mesh = loadColladaMesh(names[i], i, names.length);
	}
	
	function loadColladaMesh(name, index, total) {
		var manager = new THREE.LoadingManager();
		var loader = new THREE.ColladaLoader(manager);
		
		var mesh;
		loader.load(String.format('models/{0}.dae', name), function(collada)
		{
	        mesh = collada.scene;
			
	        mesh.scale.set(OBJECT_SCALE_UNSELECTED, OBJECT_SCALE_UNSELECTED, OBJECT_SCALE_UNSELECTED);
			mesh.rotation.x = -(Math.PI / 2); // Patch: Our models are rotated 90 degrees around the X axis, compensate for that
			
			// Position the model in the scene according to its index
			var x = -OBJECTS_START_SPREAD + (OBJECTS_START_SPREAD * 2 / (total - 1)) * index;
			positionMeshOn2DPlane(mesh, x, OBJECTS_START_Z_POS);
			
			// Store the model and add it to the scene
			meshes[index] = mesh;
			scene.add(mesh);
    	});
	}
	
	function createCube(color, size, opacity) {
		var geometry = new THREE.CubeGeometry(size, size, size);
		var material = new THREE.MeshBasicMaterial({ color: color, transparent: true, opacity: opacity });
		var cube = new THREE.Mesh(geometry, material);
		scene.add(cube);
		return cube;
	}
	
	function createLine(axis, color, size, opacity) {
		var sizeX = (axis == "x" ? 99999 : size);
		var sizeY = (axis == "y" ? 99999 : size);
		var sizeZ = (axis == "z" ? 99999 : size);
		var geometry = new THREE.CubeGeometry(sizeX, sizeY, sizeZ);
		var material = new THREE.MeshBasicMaterial({ color: color, transparent: true, opacity: opacity });
		var cube = new THREE.Mesh(geometry, material);
		scene.add(cube);
		return cube;
	}

	/* Positioning and Scaling */

	function positionMeshOn2DPlane(mesh, x, z) {
		if (!mesh) return;
		mesh.position.x = x;
		mesh.position.z = z;
	}
	
	function scaleMesh(mesh, scale) {
		if (!mesh) return;
		mesh.scale.x = scale;
		mesh.scale.y = scale;
		mesh.scale.z = scale;
	}
	
	/* Collision Detection */
	
	function findCollidingMesh(pointer) {
		var shortestDistance = Number.MAX_VALUE;
		var closestMesh;
		
		meshes.forEach(function(mesh)
		{
			var distance = getDistanceBetweenMeshes(pointer, mesh);
			if (distance < shortestDistance)
			{
				shortestDistance = distance;
				closestMesh = mesh;
			}
		});
		
		return (shortestDistance <= POINTER_COLLISION_DISTANCE_THRESHOLD ? closestMesh : null);
	}
	
	function getDistanceBetweenMeshes(mesh1, mesh2) {
		var xDistance = mesh1.position.x - mesh2.position.x;
		var yDistance = mesh1.position.y - mesh2.position.y;
		var zDistance = mesh1.position.z - mesh2.position.z;
		return Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2) + Math.pow(zDistance, 2));
	}

	/* ########################################## Leap Motion ############################################# */

	/* Filtering and Gestures */

	function filterOutHandFalsePositives(hands) {
		// Change this to correspond to the chosen interaction method.
		// Currently, we simply discard hands with one finger or less.
		return Enumerable.From(hands).Where('$.fingers.length > 1').ToArray();
	}

	function discriminateLeftHandFromRightHand(hands) {
		// No hand, return a valid but empty dictionary
		if (hands.length == 0)
			return { LEAP_LEFT_HAND : null, LEAP_RIGHT_HAND : null };
	
		// If there's only one hand, consider its horizontal position from the center to infer which one it is
		if (hands.length == 1)
		{
			var inferredKind;
			var hand = hands[0];
		
			var handKind = handKindMapping[hand.id];
			if (handKind)
			{
				// If our index already has a hand-id / hand kind mapping, don't infer
				// the kind again and simply consider that the kind didn't change.
				inferredKind = handKind;
			}
			else
			{
				// Infer the hand's kind based on its horizontal position from the center
				inferredKind = (hand.stabilizedPalmPosition[0] < 0) ? LEAP_LEFT_HAND : LEAP_RIGHT_HAND;
			
				// Store the hand association that we inferred so that we can keep that association if the hand crosses the "zero barrier"
				handKindMapping[hands[0].id] = inferredKind;
			}
		
			return { LEAP_LEFT_HAND : (inferredKind == LEAP_LEFT_HAND ? hand : null),
				     LEAP_RIGHT_HAND : (inferredKind == LEAP_RIGHT_HAND ? hand : null)};
		}
	
		// If there's two hands, order hands by their horizontal position to find the leftmost and rightmost hand
		var orderedHands = Enumerable.From(hands).OrderBy('$.stabilizedPalmPosition[0]').ToArray();
		return { LEAP_LEFT_HAND : orderedHands[0], LEAP_RIGHT_HAND : orderedHands[1] };
	}

	function isPinching(hand) {
		var fingers = getThumbAndIndexFingers(hand);
		if (fingers.length < 2) return false;
	
		var distanceBetweenFingers = getDistanceBetweenFingers(fingers[0], fingers[1]);
		
		if (pinching)
		{
			return (distanceBetweenFingers <= LEAP_PINCH_THRESHOLD_IN_MILLIMETERS + LEAP_PINCH_HYSTERESIS_MARGIN);
		}
		else
		{
			return (distanceBetweenFingers <= LEAP_PINCH_THRESHOLD_IN_MILLIMETERS);
		}
	}

	/* Helper Functions */

	function getHandKind(hand) {
		return handKindMapping[hand.id];
	}

	function getThumbAndIndexFingers(hand) {
		var kind = getHandKind(hand);
		var orderedFingersEnumerable = Enumerable.From(hand.fingers).OrderBy('$.tipPosition[0]');
	
		if (kind == LEAP_LEFT_HAND)
		{
			return orderedFingersEnumerable.TakeFromLast(2).ToArray();
		}
		else
		{
			return orderedFingersEnumerable.Take(2).ToArray();
		}
	}

	function getDistanceBetweenFingers(finger1, finger2) {
		var xDistance = finger2.tipPosition[0] - finger1.tipPosition[0];
		var yDistance = finger2.tipPosition[1] - finger1.tipPosition[1];
		var zDistance = finger2.tipPosition[2] - finger1.tipPosition[2];
		return Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2) + Math.pow(zDistance, 2));
	}
});